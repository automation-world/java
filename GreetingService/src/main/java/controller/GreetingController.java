package controller; 
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.web.bind.annotation.CrossOrigin; 
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RequestParam; 
import org.springframework.web.bind.annotation.RestController; 
import model.Greeting; 
import service.GreetingService; 
import util.Translator; 
@RestController
 @RequestMapping("/api") 
public class GreetingController { 	
@Autowired 	
private GreetingService commonService; 		
@CrossOrigin 	
@RequestMapping(value = "/greeting", method = RequestMethod.GET) 	
public Greeting executeGreeting(@RequestParam String name) { 	
	String message = ""; 		
try { 			
message = Translator.toLocale("hello"); 		
} catch (Exception e) { 			
e.getStackTrace(); 		
} 		
return commonService.executeGreeting(message, name); 	
} 
}




