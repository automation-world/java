package util;

import org.Springframework.beans.factory.annotation.Autowired;
import org.Springframework.context.i18n.LocaleContextHolder;
import org.Springframework.context.support.ResourceBundleMessageSource;
import org.Springframework.stereotype.Component;

import java.util.Locale;

@Component
public class Translator {
	private static ResourceBundleMessageSource messageSource;

	@Autowired
	Translator(ResourceBundleMessageSource messageSource) {
		Translator.messageSource = messageSource;
	}

	public static String toLocale(String msg) {
		Locale locale = LocaleContextHolder.getLocale();
		return messageSource.getMessage(msg, null, locale);
	}
}
