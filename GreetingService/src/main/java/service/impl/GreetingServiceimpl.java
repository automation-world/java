package service.impl; 
import java.util.concurrent.atomic.AtomicLong; 
import org.springframework.stereotype.Service; 
model.Greeting; import service.GreetingService; 
@Service public class GreetingServiceImpl implements GreetingService {  
private final AtomicLong counter = new AtomicLong();  
@Override  public Greeting executeGreeting(String msg, String content)
 {  
 return new Greeting(counter.incrementAndGet(), String.format(msg, content)); 
 } 
}
