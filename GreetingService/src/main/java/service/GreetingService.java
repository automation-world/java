package service;

import model.Greeting;

public interface GreetingService {

	public Greeting executeGreeting(String msg, String content);
}
