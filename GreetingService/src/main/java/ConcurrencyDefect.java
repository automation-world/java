public class ConcurrencyDefect {
	private static int refCount = 0;
	private object mutex = new object();

	public void doStuff() {
		synchronized (mutex) {
			refCount++;
		}
	}
}
